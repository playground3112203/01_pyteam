# -*- coding: utf-8 -*-
"""
Created on Wed May 25 17:09:03 2022

@author: brenda_liu
2022.06.28 突然出現urlilib3 error, 修復方式：pip3 install --upgrade requests
"""

import pandas as pd
import time
import re
from bs4 import BeautifulSoup  #!pip install beautifulsoup4
#! pip install selenium
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.chrome.service import Service
from webdriver_manager.chrome import ChromeDriverManager

import random
delay_choices = [9, 10, 12, 13, 15, 16]  #延遲的秒數
delay = random.choice(delay_choices)  #隨機選取秒數

##---- chrome
options = Options()
options.add_argument("--disable-notifications") #關掉彈跳視窗
# options.binary_location = "/usr/bin/chromium-browser"    #chrome binary location specified here
options.add_argument('--no-sandbox')
## 無介面模式:
options.add_argument('--headless')
driver = webdriver.Chrome(service=Service(ChromeDriverManager().install()), options=options)
print('Successfully enable chrome driver.')

##---- pcHome
prd_url = 'https://24h.pchome.com.tw/'
driver.get(prd_url)
driver.implicitly_wait(delay)
soup = BeautifulSoup(driver.page_source, features="html.parser")
popular_buy_pc = soup.find_all('ul', class_='c-listInfo__list')[0].find_all(class_ = 'o-prodInfo o-prodInfo--vertical')

##[暢銷排行]
prd_name_list = []
prd_price_list = []
url_list = []
img_all = []
c_pid_list = []

for i in range(0,len(popular_buy_pc)):

    price = popular_buy_pc[i].find(class_="o-prodInfo__price").text #售價
    price = price.replace("$","").replace(",","")
    price = int(price)
    prd_price_list.append(price)
    
    prd_name = popular_buy_pc[i].find(class_="o-prodInfo__title").text #商品名稱
    prd_name_list.append(prd_name)
    
    ##
    t = popular_buy_pc[i].find('a')
    t = str(t)
    img = re.findall(r'src="(.*)"/>', t)[0] # 商品圖片
    img_all.append(img)
    
    url = re.findall(r'href="(.*)" id=', t)[0] # 商品網址
    url_list.append(url)
    
    pid = re.findall(r'items/([A-Za-z0-9]*)/', t)[0] #商品編號
    c_pid_list.append(pid)
    
popular_buy_pchome = pd.DataFrame({'c_pid':c_pid_list, 'prd':prd_name_list, 'price':prd_price_list,'url':url_list, 'img':img_all})
popular_buy_pchome['c_web'] = 'pchome'
popular_buy_pchome['c_section'] = 'buy'
print('Web Crawler: popular_buy_pchome checked.')


driver.quit()

print(popular_buy_pchome)


