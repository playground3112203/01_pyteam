import requests
import json
import pandas as pd
import re
from itertools import islice

'''
說明:
get_pchome_popular()回傳暢銷排行的dataframe
get_pchome_hotsales()回傳熱銷補貨的dataframe

dataframe欄位說明:
c_pid-商品編號
prd-商品名稱
price-商品價格
url-商品網址
img-商品圖片

'''
def get_pchome_popular():
    url = 'https://ecapi-cdn.pchome.com.tw/fsapi/ecshop/composition/web/index/v1/data'
    headers = {
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36'
    }
    
    try:
        r = requests.get(url, headers=headers)
        #檢查響應狀態碼
        if r.status_code != 200:
            print('API Not Found')
            return None
        
        r = requests.get(url, headers=headers)
        data = json.loads(r.text)

        hot_data = data['hot']['Blocks']

        url = 'https://24h.pchome.com.tw/prod/'
        url_img = 'https://img.pchome.com.tw/cs'

        # 創建一個空的 DataFrame
        popular_pc = pd.DataFrame()

        for block in islice(hot_data, 6):
            node = block['Nodes'][0]# 取得第一個 node
            # 將 node 的資訊添加到 DataFrame
            data_to_append = pd.DataFrame({
                'c_pid': [node['Link']['Url']],  # 商品編號
                'prd': [node['Link']['Text']],  # 商品名稱
                'price': [node['Link']['Text1']],  # 商品價格
                'url': [url + node['Link']['Url']],  # 商品網址
                'img': [url_img + node['Img']['Src']], # 商品圖片
                })

            popular_pc = pd.concat([popular_pc, data_to_append], ignore_index = True)   
            pd.set_option('display.max_colwidth', None)
        return popular_pc
        
    except Exception as e:
        print('Error:', e)
        return None


def get_pchome_hotsales():
    url = 'https://ecapi-cdn.pchome.com.tw/ecshop/prodapi/v2/replenish/prod&_callback=jsonpcb_replenish&4745771?limit=14&_callback=jsonpcb_replenish'
    
    headers = {
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36'
    }
    try:
        r = requests.get(url, headers=headers)
        #檢查響應狀態碼
        if r.status_code != 200:
            print('API Not Found')
            return None
        
        #提取 jsonpcb_replenish裡的資料
        match = re.search(r'jsonpcb_replenish\((.*?)\);', r.text, re.DOTALL)
        if not match:
            print('Data Not Match')
            return None
        
        json_data = match.group(1).strip() #去除匹配到的字串兩端的空白字符

        #解析JSON資料
        hotsales_data = json.loads(json_data)
        url = 'https://24h.pchome.com.tw/prod/'
        url_img = 'https://img.pchome.com.tw/cs'
        hotsales_df = pd.DataFrame()
        for item in hotsales_data:
            data_to_append = pd.DataFrame({
                'c_id': [item['Id']],    # 商品編號
                'prd': [item['Name']],    # 商品名稱
                'price': [item['Price']['P']], # 商品價格
                'url': [url + item['Id']],     # 商品網址
                'img': [url_img + item['Pic']['S']]  # 商品圖片
            })        
            #將 data_to_append 加到 hotsales_df 裡，不保留 data_to_append 索引
            hotsales_df = pd.concat([hotsales_df, data_to_append], ignore_index=True)
            pd.set_option('display.max_colwidth', None)
        return hotsales_df
    
    except Exception as e:
        print('Error:', e)
        return None

print(get_pchome_popular())
print(get_pchome_hotsales())