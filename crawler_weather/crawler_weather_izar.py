import requests
import pandas as pd
from sqlalchemy import create_engine
import numpy as np
from fastapi import FastAPI, HTTPException
import uvicorn
import logging

'''
get_weather_data(county)
county輸入縣市名稱
return 該縣市的dataframe

df_to_database()
將各縣市的dataframe存進database

get_warning_data()
把警特報的dataframe存去資料庫

get_earthquake_data()
把地震的dataframe存去資料庫

clean_data(df)
清理資料，將NaN替換成'None'

recommend_product(county,area)
county輸入縣市
area輸入區域
天氣方面:顯示該縣市區域一週內低溫、高溫、下雨、強風、紫外線高時段，並顯示對應推薦的商品資料
特報方面:顯示該縣市特報濃霧、大雨、豪雨、大豪雨、超大豪雨,、陸上強風、海上陸上颱風時段，並顯示對應的推薦商品資料。若沒有資料則顯示當前縣市無警特報。
地震方面:篩選該縣市兩週內地震時段顯示，並顯示推薦商品。若該縣市一週內無地震顯示近兩週沒有地震


'''
def get_weather_data(county):
    dic_county = {'宜蘭縣': '03', '桃園市': '07', '新竹縣': '11', '苗栗縣': '15', '彰化縣': '19', '南投縣': '23',
                '雲林縣': '27', '嘉義縣': '31', '屏東縣': '35', '臺東縣': '39', '花蓮縣': '43', '澎湖縣': '47',
                '基隆市': '51', '新竹市': '55', '嘉義市': '59', '臺北市': '63','高雄市': '67', '新北市': '71',
                '臺中市': '75', '臺南市': '79', '連江縣': '83', '金門縣': '87'}

    if county in dic_county:
        api_url = f"https://opendata.cwa.gov.tw/api/v1/rest/datastore/F-D0047-0{dic_county[county]}?Authorization=CWA-441C96CF-64CA-49A9-A924-4A2268C0A6A1"

        try:
            response = requests.get(api_url)
            if response.status_code == 200:
                data = response.json()
                weather_df = pd.DataFrame()  # 初始化 DataFrame

                for location_info in data['records']['locations'][0]['location']:
                    location = location_info['locationName']
                    for i in range(len(location_info['weatherElement'][0]['time'])):
                        start_time = pd.to_datetime(location_info['weatherElement'][0]['time'][i]['startTime'])
                        uvi = location_info['weatherElement'][9]['time']
                        uvi_value = None  # 設置默認值為 None
            
                        for uvi_data in uvi:
                            uvi_start_time = pd.to_datetime(uvi_data['startTime'])
                            if uvi_start_time == start_time:
                                uvi_value = uvi_data['elementValue'][0]['value']
                                break  # 找到匹配的開始時間後立即退出循環
                            
                        data_to_append = {
                            'county':county,
                            'location': location,
                            'startTime': start_time,                                                                #起始時間
                            'endTime': pd.to_datetime(location_info['weatherElement'][0]['time'][i]['endTime']),    #結束時間
                            'pop12h': location_info['weatherElement'][0]['time'][i]['elementValue'][0]['value'].strip() or None,  #12小時降雨機率
                            'MinT': location_info['weatherElement'][8]['time'][i]['elementValue'][0]['value'],          #最低溫度 
                            'MaxT': location_info['weatherElement'][12]['time'][i]['elementValue'][0]['value'],         #最高溫度
                            'RH': location_info['weatherElement'][2]['time'][i]['elementValue'][0]['value'],            #平均相對溼度
                            'WS': location_info['weatherElement'][4]['time'][i]['elementValue'][0]['value'],            #最大風速
                            'Beaufort scale': location_info['weatherElement'][4]['time'][i]['elementValue'][1]['value'],      #蒲福風級
                            'MinAT': location_info['weatherElement'][11]['time'][i]['elementValue'][0]['value'],        #最低體感溫度
                            'MaxAT': location_info['weatherElement'][5]['time'][i]['elementValue'][0]['value'],         #最高體感溫度
                            'Wx': location_info['weatherElement'][6]['time'][i]['elementValue'][0]['value'],            #天氣現象
                            'UVI_value': uvi_value,                                                                      #紫外線指數
                        }
                        weather_df = pd.concat([weather_df, pd.DataFrame(data_to_append, index=[0])], ignore_index=True)
                
                return weather_df
            else:
                print(f"API 請求失敗，狀態碼: {response.status_code}")
                return None

        except Exception as e:
            print(f"獲取 {api_url} 資料時發生異常: {e}")
            return None
    else:
        print("未找到對應的縣市名稱")
        return None
    
def df_to_database():
    
    # 設置資料庫連接
    engine = create_engine('sqlite:///weather_data.db')
    
    # 創建一個空的 DataFrame 來存儲所有縣市的天氣資料
    all_weather_data = pd.DataFrame()

    county_dict = {
        # 您的縣市字典
        '宜蘭縣': 'df_yilancounty',
        '桃園市': 'df_taoyuancity',
        '新竹縣': 'df_hsinchucounty',
        '苗栗縣': 'df_miaolicounty',
        '彰化縣': 'df_changhuacounty', 
        '南投縣': 'df_nantoucounty',
        '雲林縣': 'df_yunlincounty',
        '嘉義縣': 'df_chiayicounty', 
        '屏東縣': 'df_pingtungcounty',
        '臺東縣': 'df_taitungcounty',
        '花蓮縣': 'df_hualiencounty',
        '澎湖縣': 'df_penghucounty',
        '基隆市': 'df_keelungcounty',
        '新竹市': 'df_hsinchucity', 
        '嘉義市': 'df_chiayicounty',
        '臺北市': 'df_taipeicity',
        '高雄市': 'df_kaoshiungcity',
        '新北市': 'df_newtaipeicity',
        '臺中市': 'df_taichangcity',
        '臺南市': 'df_tainancity',
        '連江縣': 'df_lienchangcounty',
        '金門縣': 'df_kinmencounty'
    }

    for county, dataframe_name in county_dict.items():
        df = get_weather_data(county)
        if df is not None:
            all_weather_data = pd.concat([all_weather_data, df], ignore_index=True)

    try:
        # 將 DataFrame 存儲到數據庫表中
        all_weather_data.to_sql('all_weather_data', engine, if_exists='replace', index=False)
        all_weather_data.fillna(np.nan, inplace=True)
        print("所有縣市天氣資料已成功存儲。")
    except Exception as e:
        print(f"存儲所有縣市天氣資料時出錯: {e}")

    print("所有資料都成功存儲。")
    
def get_warning_data():
    url="https://opendata.cwa.gov.tw/api/v1/rest/datastore/W-C0033-001"
    params = {
        "Authorization": "CWA-441C96CF-64CA-49A9-A924-4A2268C0A6A1",
    }
    response = requests.get(url, params=params)
    data = response.json()

    warning_df = pd.DataFrame()

    
    for location in data["records"]["location"]:
        locationName = location["locationName"]
        hazards = location["hazardConditions"]["hazards"]
        if not hazards:  # 如果 hazards 是空列表
            data_to_append = {
                "location": locationName,  # 縣市地點
                "phenomena": None,  # 特報 
                "startTime": None,  # 開始時間
                "endTime": None  # 結束時間               
            }
        else:
            for hazard in hazards:
                data_to_append = {
                    "location": locationName,  # 縣市地點
                    "phenomena": hazard["info"]["phenomena"],  # 特報 
                    "startTime": pd.to_datetime(hazard["validTime"]["startTime"]),  # 開始時間
                    "endTime": pd.to_datetime(hazard["validTime"]["endTime"])  # 結束時間  
                }        
        warning_df = pd.concat([warning_df, pd.DataFrame(data_to_append, index=[0])], ignore_index=True)        

    # 設置資料庫連接
    engine = create_engine('sqlite:///weather_data.db')
    # 存儲到資料庫
    warning_df.to_sql('warning_data', engine, if_exists='replace', index=False)
    return warning_df
def get_earthquake_data():
    url = "https://opendata.cwa.gov.tw/api/v1/rest/datastore/E-A0015-001"
    params = {
        "Authorization": "CWA-441C96CF-64CA-49A9-A924-4A2268C0A6A1",
    }
    response = requests.get(url, params=params)
    data = response.json()

    records = data["records"]["Earthquake"]
    earthquake_data = [
        {
            "時間": pd.to_datetime(record["EarthquakeInfo"]["OriginTime"]),
            "芮氏規模": record["EarthquakeInfo"]["EarthquakeMagnitude"]["MagnitudeValue"],
            "區域": shaking_data["CountyName"],
            "震度": shaking_data["AreaIntensity"],
            "震度顏色": record["ReportColor"]  
        }
        for record in records
        for shaking_data in record["Intensity"]["ShakingArea"]
    ]

    earthquake_df = pd.concat([pd.DataFrame([data]) for data in earthquake_data], ignore_index=True)

    # 設置資料庫連接
    engine = create_engine('sqlite:///weather_data.db')
    # 存儲到資料庫
    earthquake_df.to_sql('earthquake_data', engine, if_exists='replace', index=False)
    return earthquake_df

# 設置日誌
logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)


def replace_invalid_values(obj):
            if isinstance(obj, float) and (np.isnan(obj) or np.isinf(obj)):
                return None
            elif isinstance(obj, dict):
                return {k: replace_invalid_values(v) for k, v in obj.items()}
            elif isinstance(obj, list):
                return [replace_invalid_values(item) for item in obj]
            else:
                return obj
def recommend_product(county, area):
    try:

        logger.info("Connecting to database...")
        # 設置資料庫連接
        engine = create_engine('sqlite:///weather_data.db')

        logger.info("Reading weather data from database...")
        # 讀取資料庫中的天氣資料
        all_weather_data = pd.read_sql_table('all_weather_data', engine)

        logger.info("Reading weather product CSV file...")
        # 讀取 CSV 文件並建立 DataFrame
        w_p = pd.read_csv('df_weather_product.csv', encoding='Big5')
        w_p.columns = ['L', 'Category1', 'M', 'Category2', 'B', 'Category3', 'B2', 'Category4', 'Check', 'Weather_Type']
        w_p.to_sql('df_weather_product', engine, if_exists='replace', index=False)
        w_p = pd.read_sql_table('df_weather_product', engine)

        logger.info("Filtering weather data for area and county...")
        # 篩選一週内符合條件的天氣資料
        area_data = all_weather_data[(all_weather_data['location'] == area) & (all_weather_data['county'] == county)]

        logger.info("Setting up weather conditions...")
        conditions = [
            area_data['Wx'].str.contains('雨'),
            (area_data['MinT'].astype(float) <= 15) & area_data['MinT'].notnull(),
            (area_data['MaxT'].astype(float) >= 28) & area_data['MaxT'].notnull(),
            (area_data['Beaufort scale'].str.strip() == '>= 6') & area_data['Beaufort scale'].notnull(),
            (area_data['UVI_value'].astype(float) >= 6) & area_data['UVI_value'].notnull(),
            # 添加其他天氣條件
        ]

        # 存儲結果
        result = {
            'weather': {},
            'warnings': {},
            'earthquakes': {}
        }

        # 存儲符合條件的商品
        recommended_products_weather = pd.DataFrame()
        category_recommendations = ['下雨', '低溫', '高溫', '強風', '紫外線']

        matching_conditions = []  # 用於記錄符合條件的天氣類別

        logger.info("Filtering weather-related products...")
        #篩選各天氣現象符合的商品
        for i, cond in enumerate(conditions):
            category = category_recommendations[i]
            if cond.any():
                matching_conditions.append(category)  #記錄符合的天氣條件
                result['weather'][category] = area_data[cond].to_dict(orient='records')
                categories = w_p[w_p['Weather_Type'].str.contains(category, na=False)]
                if not categories.empty:
                    result['weather'][f"{category}_products"] = categories.to_dict(orient='records')
                    recommended_products_weather = pd.concat([recommended_products_weather, categories])
            else:
                result['weather'][category] = f"一週內沒有{category}"

        result['weather']['matching_conditions'] = matching_conditions  # 將符合的天氣條件加入結果中

        recommended_products_weather = recommended_products_weather.drop_duplicates()
        result['weather']['all_recommended_products'] = recommended_products_weather.to_dict(orient='records')

        logger.info("Reading warning data from database...")
        # 讀取警特報資料並篩選指定縣市
        warning_df = pd.read_sql_query(f"SELECT * FROM warning_data WHERE location = '{county}'", engine)

        # 初始化推薦商品 DataFrame
        recommended_products_warning = pd.DataFrame()
        category_recommendations = {'濃霧': '霧', '大雨': '雨', '豪雨': '雨', '大豪雨': '雨', '超大豪雨': '雨', '陸上強風': '強風', '海上陸上颱風': '颱風'}

        matching_warning_conditions = []  # 用於記錄符合條件的警特報類別

        logger.info("Filtering warning-related products...")
        if warning_df['phenomena'].notnull().any():
            for category, weather_type in category_recommendations.items():
                if warning_df['phenomena'].str.contains(category).any():
                    matching_warning_conditions.append(category)  #記錄符合的警特報條件
                    result['warnings'][category] = warning_df[warning_df['phenomena'].str.contains(category)].to_dict(orient='records')
                    products = pd.read_sql_query(f"SELECT * FROM df_weather_product WHERE Weather_Type LIKE '%{weather_type}%'", engine)
                    result['warnings'][f"{category}_products"] = products.to_dict(orient='records')
                    recommended_products_warning = pd.concat([recommended_products_warning, products])

            recommended_products_warning = recommended_products_warning.drop_duplicates()
            result['warnings']['matching_conditions'] = matching_warning_conditions  # 將符合的警特報條件加入結果中
            result['warnings']['all_recommended_products'] = recommended_products_warning.to_dict(orient='records')
        else:
            result['warnings']['none'] = f"{county} 沒有發布警特報。"

        logger.info("Reading earthquake data from database...")
        # 讀取地震資料并篩選指定縣市
        earthquake_df = pd.read_sql_table('earthquake_data', engine)
        county_df = earthquake_df[earthquake_df['區域'] == county]

        # 取得今天時間
        current_date = pd.Timestamp('today')

        # 篩選近兩週的地震資料
        past_week_df = county_df[pd.to_datetime(county_df['時間']) >= current_date - pd.DateOffset(weeks=2)]

        logger.info("Filtering earthquake-related products...")
        if len(past_week_df) == 0:
            result['earthquakes']['none'] = "近兩週沒有地震"
        else:
            result['earthquakes']['recent'] = past_week_df.to_dict(orient='records')
            earthquake_product = w_p[w_p['Weather_Type'].str.contains('地震', na=False)]
            result['earthquakes']['products'] = earthquake_product.to_dict(orient='records')
        
        # 替換無效值
        for key, value in result.items():
            result[key] = {k: replace_invalid_values(v) for k, v in value.items()}
                
        return result

    except Exception as e:
        logger.error(f"An error occurred: {str(e)}")
        return {"error": str(e)}
        
    
#FastAPI相關程式碼 
app = FastAPI(
    title="氣象FastAPI"
) 

@app.on_event("startup")
async def startup_event():
    # Pre-load data before the app starts
    logger.info("Running df_to_database() to pre-load weather data.")
    df_to_database()
    logger.info("Running get_earthquake_data() to pre-load earthquake data.")
    get_earthquake_data()
    logger.info("Running get_warning_data() to pre-load warning data.")
    get_warning_data()
    logger.info("Data pre-loading completed.")

# Your existing API endpoints
@app.get("/")
async def Hello():
    return {"message": "Welcome to the Weather API"}

@app.get("/get_weather_data/{county}")
async def get_weather_data_endpoint(county: str):
    df = get_weather_data(county)
    return df.to_dict(orient='records')

@app.get("/get_warning_data")
async def get_warning_data_endpoint():
    df = get_warning_data()
    return df.to_dict(orient='records')

@app.get("/get_earthquake_data")
async def get_earthquake_data_endpoint():
    df = get_earthquake_data()
    return df.to_dict(orient='records')

@app.get("/recommend_product/{county}&{area}")
async def recommend_product_endpoint(county: str, area: str):
    result = recommend_product(county, area)
    return result

if __name__ == '__main__':
    uvicorn.run("crawler_weather_izar:app", reload=False)   

