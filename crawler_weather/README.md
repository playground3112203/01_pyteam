# 氣象商品推薦
## 專案目的
根據最近氣象狀況來推薦商品類別。

## 下載特定套件

```bash
python.exe -m pip install -r requirements.txt
```

## 執行步驟
```bash
python crawler_weather/crawler_weather_izar.py
```

## 氣象 API


### 取得縣市一周天氣
```http
GET /get_weather_data/{county}
```

get_weather_data，參數county(打縣市名稱，臺不能打台)。
```bash
curl -X 'GET' 'http://localhost:8000/get_weather_data/{county}' -H 'accept: application/json'
```

Response Body Example
```bash
[
  {
    "county": "臺北市",
    "location": "南港區",
    "startTime": "2024-06-21T12:00:00",
    "endTime": "2024-06-21T18:00:00",
    "pop12h": "50",
    "MinT": "31",
    "MaxT": "35",
    "RH": "79",
    "WS": "4",
    "Beaufort scale": "3",
    "MinAT": "36",
    "MaxAT": "42",
    "Wx": "多雲午後短暫雷陣雨",
    "UVI_value": "10"
  },...
]
```

### 取得警特報資料
```http
GET /get_warning_data
```
get_warning_data，沒有參數。
```bash
curl -X 'GET' \
  'http://127.0.0.1:8000/get_warning_data' \
  -H 'accept: application/json'
```
Response Example:
```bash
[
   {
    "location": "臺東縣",
    "phenomena": null,
    "startTime": "NaT",
    "endTime": "NaT"
  },
  {
    "location": "基隆市",
    "phenomena": "大雨",
    "startTime": "2024-06-21T13:59:00",
    "endTime": "2024-06-21T23:00:00"
  },
  {
    "location": "苗栗縣",
    "phenomena": "大雨",
    "startTime": "2024-06-21T13:59:00",
    "endTime": "2024-06-21T23:00:00"
  },...
]
```

### 取得地震資料
```http
GET /get_earthquake_data
```

get_earthquake_data，沒有參數。
```bash
curl -X 'GET' \
  'http://127.0.0.1:8000/get_earthquake_data' \
  -H 'accept: application/json'
```
Response Example:
```bash
[
  {
    "時間": "2024-06-21T12:25:56",
    "芮氏規模": 4.6,
    "區域": "南投縣",
    "震度": "2級",
    "震度顏色": "綠色"
  },
  {
    "時間": "2024-06-21T12:25:56",
    "芮氏規模": 4.6,
    "區域": "南投縣、嘉義縣",
    "震度": "2級",
    "震度顏色": "綠色"
  },...
]
```

### 取得氣象推薦商品
```http
GET /recommend_product/{county}&{area}
```

recommend_product，參數county(打縣市名稱，臺不能打台)，area(打區域名稱)。
```bash
curl -X 'GET' \
  'http://127.0.0.1:8000/recommend_product/{county}&{area}' \
  -H 'accept: application/json'
```
Response Example:
```bash
{
 "weather": 
 { 
    "下雨": [
    {
      "county": "臺北市",
      "location": "內湖區",
      "startTime": "2024-06-21T12:00:00",
      "endTime": "2024-06-21T18:00:00",
      "pop12h": "50",
      "MinT": "32",
      "MaxT": "36",
      "RH": "76",
      "WS": "3",
      "Beaufort scale": "2",
      "MinAT": "37",
      "MaxAT": "42",
      "Wx": "多雲午後短暫雷陣雨",
      "UVI_value": "10"
  },...],
    "下雨_products": [
    {
      "L": "L0001",
      "Category1": "家電．電視．冷氣．冰箱",
      "M": "M0096",
      "Category2": "季節家電",
      "B": "B0150",
      "Category3": "除濕機",
      "B2": null,
      "Category4": null,
      "Check": "V",
      "Weather_Type": "低溫、下雨、霧、颱風"
    },...],
    "低溫": "一週內沒有低溫",
    "高溫": [
    {
      "county": "臺北市",
      "location": "內湖區",
      "startTime": "2024-06-21T12:00:00",
      "endTime": "2024-06-21T18:00:00",
      "pop12h": "50",
      "MinT": "32",
      "MaxT": "36",
      "RH": "76",
      "WS": "3",
      "Beaufort scale": "2",
      "MinAT": "37",
      "MaxAT": "42",
      "Wx": "多雲午後短暫雷陣雨",
      "UVI_value": "10"
    },...],
    "高溫_products": [
    {
      "L": "L0001",
      "Category1": "家電．電視．冷氣．冰箱",
      "M": "M0133",
      "Category2": "冷暖空調",
      "B": "B0253",
      "Category3": "移動式空調",
      "B2": null,
      "Category4": null,
      "Check": "V",
      "Weather_Type": "低溫、高溫"
    },...],
    "強風": "一週內沒有強風",
    "紫外線": [
    {
      "county": "臺北市",
      "location": "內湖區",
      "startTime": "2024-06-21T12:00:00",
      "endTime": "2024-06-21T18:00:00",
      "pop12h": "50",
      "MinT": "32",
      "MaxT": "36",
      "RH": "76",
      "WS": "3",
      "Beaufort scale": "2",
      "MinAT": "37",
      "MaxAT": "42",
      "Wx": "多雲午後短暫雷陣雨",
      "UVI_value": "10"
    },...],
    "紫外線_products": [
    {
      "L": "L0004",
      "Category1": "家具．家飾．衛浴．燈飾",
      "M": "M0011",
      "Category2": "家飾．織品",
      "B": "B0011",
      "Category3": "窗簾/門簾",
      "B2": null,
      "Category4": null,
      "Check": "V",
      "Weather_Type": "高溫、紫外線"
    },...],
    "matching_conditions": [
    "下雨",
    "高溫",
    "紫外線"
    ],
    "all_recommended_products": [
    {
      "L": "L0001",
      "Category1": "家電．電視．冷氣．冰箱",
      "M": "M0096",
      "Category2": "季節家電",
      "B": "B0150",
      "Category3": "除濕機",
      "B2": null,
      "Category4": null,
      "Check": "V",
      "Weather_Type": "低溫、下雨、霧、颱風"
    },...]
    },
    "warnings": 
    {
      "大雨": [
      {
        "location": "臺北市",
        "phenomena": "大雨",
        "startTime": "2024-06-21 13:59:00.000000",
        "endTime": "2024-06-21 23:00:00.000000"
      }
    ],
    "大雨_products": [
      {
        "L": "L0001",
        "Category1": "家電．電視．冷氣．冰箱",
        "M": "M0096",
        "Category2": "季節家電",
        "B": "B0150",
        "Category3": "除濕機",
        "B2": null,
        "Category4": null,
        "Check": "V",
        "Weather_Type": "低溫、下雨、霧、颱風"
      },...],
    "matching_conditions": [
      "大雨"
    ],
    "all_recommended_products": [
      {
        "L": "L0001",
        "Category1": "家電．電視．冷氣．冰箱",
        "M": "M0096",
        "Category2": "季節家電",
        "B": "B0150",
        "Category3": "除濕機",
        "B2": null,
        "Category4": null,
        "Check": "V",
        "Weather_Type": "低溫、下雨、霧、颱風"
      },...]
    },
    "earthquakes": 
    {
        "none": "近兩週沒有地震"
    }
}
```