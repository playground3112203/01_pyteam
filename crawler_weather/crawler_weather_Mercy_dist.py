# 這檔案是抓取氣象平台 API 三個縣市地區資料
import requests
# 匯入 requests 模組

def get_data():
# 定義一個函數 get_data  

    url = "https://opendata.cwa.gov.tw/api/v1/rest/datastore/F-D0047-093"
    # 設定抓取 API 的 URL
    
    headers = {
        "Authorization": "CWA-F14FD326-2563-4B88-8D66-79DE28E54653",
    }
    # 設定標頭，識別身份授權碼
    
    params = {
        "locationId": "F-D0047-051,F-D0047-063,F-D0047-071",
    }
    # 讀取資料這三筆 F-D0047-051 (基隆市), F-D0047-063 (臺北市), F-D0047-071 (新北市)

    response = requests.get(url, headers=headers, params=params)
    # 用get獲取url資源
    
    if response.status_code == 200:
    # 檢查狀態碼是否為 200 
        response_json = response.json()
        # 將回應的 json 資料轉換成 python 字典

        locations = response_json["records"]["locations"]
        # 從字典中取出需求範圍需要的資料

        for location in locations:
        # 迴圈處理每個縣市的資料
            
            city_name = location["locationsName"]
            # 抓取縣市資料
   
            for dist in location["location"]:
            # 迴圈處理每個地區的資料
                
                dist_name = dist["locationName"]
                # 抓取地區資料
        
                for weather in location["location"]:
                # 迴圈處理每筆天氣狀況的資料
                    weather_description = weather["weatherElement"][10]["time"][0]["elementValue"][0]["value"]
                    # 抓取天氣資料
                
                print(f"縣市: {city_name}")
                print(f"區域: {dist_name}")
                print(f"天氣狀況: {weather_description}")
                print("----------------------------------------------------------------------------------------------------------------")
                # 顯示所有天氣資訊

    else:
        print("抓取不到資料")
        # 若狀態碼不是 200，顯示錯誤訊息

get_data()
# 呼叫 get_data 函數，執行爬蟲