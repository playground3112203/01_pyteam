import pandas as pd

def show_first_student_info_loc(student_info, student_scores):
    """
    顯示student_info第一個學生的基本資料
    請使用loc
    """
    df_expected = student_info.loc[0]

    return df_expected

def show_first_student_info_iloc(student_info, student_scores):
    """
    顯示student_info第一個學生的基本資料
    請使用iloc
    """
    df_expected = student_info.iloc[0]

    return df_expected

def show_all_student_names(student_info, student_scores):
    """
    顯示所有學生的名字
    """
    df_expected = student_info['name']

    return df_expected

def merge_scores_to_info(student_info, student_scores):
    """
    合併兩者每個學生都可以多一個欄顯示成績
    """
    student_info['student_id'] = student_info['student_id'].astype(int)
    df_expected = student_info.merge(student_scores, on='student_id', how='left')

    return df_expected

def set_missing_scores_to_N(student_info, student_scores):
    """
    若沒有成績者df需顯示為"N"
    """
    student_info['student_id'] = student_info['student_id'].astype(int)
    df_expected = student_info.merge(student_scores, on='student_id', how='left')
    return df_expected

def show_male_students(student_info, student_scores):
    """
    顯示合併後所有男學生的資料
    """
    student_info['student_id'] = student_info['student_id'].astype(int)
    merged_df = student_info.merge(student_scores, on='student_id', how='left')
    df_expected = merged_df[merged_df['gender'] == 'M']

    return df_expected

def show_female_high_scores(student_info, student_scores):
    """
    顯示合併後所有女學生且分數高於等於90分的資料
    """
    student_info['student_id'] = student_info['student_id'].astype(int)
    merged_df = student_info.merge(student_scores, on='student_id', how='left')
    df_expected = merged_df[(merged_df['gender'] == 'F') & (merged_df['score'] >= 90)]

    return df_expected

def sort_by_scores_desc(student_info, student_scores):
    """
    將合併後的df依照分數高至低排序
    """    
    student_info['student_id'] = student_info['student_id'].astype(int)
    df_merged = student_info.merge(student_scores, on='student_id', how='left')
    df_expected = df_merged.sort_values(by='score', ascending=False)
    return df_expected

def show_names_and_scores(student_info, student_scores):
    """
    只顯示學生姓名與成績
    """
    student_info['student_id'] = student_info['student_id'].astype(int)
    merged_df = student_info.merge(student_scores, on='student_id', how='left')
    df_expected = merged_df[['name', 'score']]

    return df_expected

def rename_score_to_exam1_score(student_info, student_scores):
    """
    顯示合併後的數列，但欄位score更改為 exam1_score
    """

    student_info['student_id'] = student_info['student_id'].astype(int)
    merged_df = student_info.merge(student_scores, on='student_id', how='left')
    return merged_df.rename(columns={'score': 'exam1_score'})

def add_pass_column(student_info, student_scores):
    """
    新增一欄pass，若小於成績中位數顯示"fail",若大於成績中位數顯示“pass”
    """
    student_info['student_id'] = student_info['student_id'].astype(int)
    merged_df = student_info.merge(student_scores, on='student_id', how='left')
    median_score = merged_df['score'].dropna().median()
    merged_df['pass'] = merged_df['score'].apply(lambda x: 'pass' if ((x >= median_score) and (pd.notnull(x))) else 'fail')
    df_expected = merged_df.copy()

    return df_expected