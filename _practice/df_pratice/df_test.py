"""
題目：
有兩個 DataFrame，一個是存儲學生基本信息的student_info，另一個是存儲學生成績的 student_scores。
請在下列十個functions中回傳題目所要的值
有些題目不只一種做法
"""
import pandas as pd

data_info = {'student_id': ['1', '2', '3', '4', '5'],
             'name': ['Alice', 'Bob', 'Charlie', 'David', 'Eva'],
             'gender': ['F', 'M', 'M', 'M', 'F']}
student_info = pd.DataFrame(data_info)
data_scores = {'student_id': [1, 2, 3, 4, 6],
               'score': [85, 90, 78, 92, 88]}
student_scores = pd.DataFrame(data_scores)

import pandas as pd

def show_first_student_info_loc(student_info, student_scores):
    """
    顯示student_info第一個學生的基本資料
    請使用loc
    """
    ...
def show_first_student_info_iloc(student_info, student_scores):
    """
    顯示student_info第一個學生的基本資料
    請使用iloc
    """
    ...

def show_all_student_names(student_info, student_scores):
    """
    顯示所有學生的名字
    """
    ...

def merge_scores_to_info(student_info, student_scores):
    """
    合併兩者每個學生都可以多一個欄顯示成績
    """
    ...

def set_missing_scores_to_N(student_info, student_scores):
    """
    若沒有成績者df需顯示為"N"
    """
    ...

def show_male_students(student_info, student_scores):
    """
    顯示合併後所有男學生的資料
    """
    ...

def show_female_high_scores(student_info, student_scores):
    """
    顯示合併後所有女學生且分數高於等於90分的資料
    """
    ...

def sort_by_scores_desc(student_info, student_scores):
    """
    將合併後的df依照分數高至低排序
    """
    ...

def show_names_and_scores(student_info, student_scores):
    """
    只顯示學生姓名與成績
    """
    ...

def rename_score_to_exam1_score(student_info, student_scores):
    """
    顯示合併後的數列，但欄位score更改為 exam1_score
    """
    ...

def add_pass_column(student_info, student_scores):
    """
    新增一欄pass，若小於成績中位數顯示"fail",若大於成績中位數顯示“pass”
    """
    ...