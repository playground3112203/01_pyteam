#!/usr/bin/env python
# coding: utf-8

# In[1]:


"""
題目：
有兩個 DataFrame，一個是存儲學生基本信息的student_info，另一個是存儲學生成績的 student_scores。
請在下列十個functions中回傳題目所要的值
有些題目不只一種做法
"""
import pandas as pd

data_info = {'student_id': ['1', '2', '3', '4', '5'],
             'name': ['Alice', 'Bob', 'Charlie', 'David', 'Eva'],
             'gender': ['F', 'M', 'M', 'M', 'F']}
student_info = pd.DataFrame(data_info)
data_scores = {'student_id': [1, 2, 3, 4, 6],
               'score': [85, 90, 78, 92, 88]}
student_scores = pd.DataFrame(data_scores)






# In[3]:


def show_first_student_info_loc(student_info, student_scores):
    """
    顯示student_info第一個學生的基本資料
    請使用loc
    """
    #loc含頭含尾
    #取第一row
    return student_info.loc[0]
show_first_student_info_loc(student_info, student_scores)


# In[4]:


def show_first_student_info_iloc(student_info, student_scores):
    """
    顯示student_info第一個學生的基本資料
    請使用iloc
    """
    #iloc含頭不含尾
    #取第一row
    return student_info.iloc[0]
show_first_student_info_iloc(student_info, student_scores)


# In[5]:


def show_all_student_names(student_info, student_scores):
    """
    顯示所有學生的名字
    """
    #取得單一欄位資料(型別為DataFrame) 
    #df[["name"]]
    
    return student_info['name']
show_all_student_names(student_info, student_scores)


# In[6]:


def merge_scores_to_info(student_info, student_scores):
    """
    合併兩者每個學生都可以多一個欄顯示成績
    """
     # 將 student_info 中的 'student_id' 列轉換為 int
    student_info['student_id'] = student_info['student_id'].astype('int64')
    #基於student_id把student_info和student_scores合併，用left模式
    student_all= pd.merge(student_info, student_scores, on='student_id', how='left')

    return student_all
merge_scores_to_info(student_info, student_scores)


#遇到問題：
#表明兩個 DataFrame 中的 'student_id' 列的數據類型不匹配。
#在 data_info DataFrame 中，'student_id' 的類型是字符串，
#而在 data_scores DataFrame 中，'student_id' 的類型是 int64。

#通過合併之前將 data_info DataFrame 中的 'student_id' 列轉換為 int64 來解決這個問題


# In[7]:


def set_missing_scores_to_N(student_info, student_scores):
    """
    若沒有成績者df需顯示為"N"
    """
    # 將 student_info 中的 'student_id' 列轉換為 int64
    student_info['student_id'] = student_info['student_id'].astype('int')
    #基於student_id把student_info和student_scores合併，用left模式
    student_all= pd.merge(student_info, student_scores, on='student_id', how='left')
     
    student_all.fillna(value='N', inplace=True)
   
    return student_all
set_missing_scores_to_N(student_info, student_scores)


# In[8]:


def show_male_students(student_info, student_scores):
    """
    顯示合併後所有男學生的資料
    """
    
    #stuent_all為合併的學生資料
    student_all=merge_scores_to_info(student_info, student_scores)
    #is_male確認學生中gender是M的資料
    is_male=(student_all['gender']=='M')
    
    return student_all[is_male]
show_male_students(student_info, student_scores)    


# In[9]:


def show_female_high_scores(student_info, student_scores):
    """
    顯示合併後所有女學生且分數高於等於90分的資料
    """
    student_all=merge_scores_to_info(student_info, student_scores)
   
    return student_all[(student_all['gender'] == 'F') & (student_all['score'] >= 90)]

show_female_high_scores(student_info, student_scores)


# In[10]:


def sort_by_scores_desc(student_info, student_scores):
    """
    將合併後的df依照分數高至低排序
    """
    student_all=merge_scores_to_info(student_info, student_scores)
     
  
   # 預設情況下，排序按升序排列，要按降序更改 DataFrame，需要設定標誌 ascending=False。
   # 排序語法:df.sort_values(by=["col1"])
    student_all=student_all.sort_values(by=['score'],ascending=False)
    
    return (student_all)
sort_by_scores_desc(student_info, student_scores)   


# In[11]:


def show_names_and_scores(student_info, student_scores):
    """
    只顯示學生姓名與成績
    """
    student_all=merge_scores_to_info(student_info, student_scores)

   #df.loc[:,['這裡寫column的名字要加引號']]
    return student_all.loc[:,['name','score']]
show_names_and_scores(student_info, student_scores)   


# In[12]:


def rename_score_to_exam1_score(student_info, student_scores):
    """
    顯示合併後的數列，但欄位score更改為 exam1_score
    """
    ...
    student_all=merge_scores_to_info(student_info, student_scores)
    #將舊column名稱指定為鍵，將新名稱指定為值。
    #重新命名 參考語法example_df.rename(columns={"Marks": "Roll_no", "Roll_no": "Marks"}, inplace=True)
    student_all.rename(columns={"score": "exam1_score"}, inplace=True)
    return student_all
rename_score_to_exam1_score(student_info, student_scores)


# In[13]:




def add_pass_column(student_info, student_scores):
    """
    新增一欄pass，若小於成績中位數顯示"fail",若大於成績中位數顯示“pass”
    中位數不計入沒有分數的值，沒有分數直接當fail
    """
    student_all=merge_scores_to_info(student_info, student_scores)
   
    #計算中位數
    median=student_all['score'].median()
    #pass欄位
    student_all['pass']= student_all['score'].apply(lambda x: 'pass' if x > median else 'fail')
   
    return student_all
add_pass_column(student_info, student_scores)



# In[ ]:




