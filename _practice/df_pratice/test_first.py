import pandas as pd
# pip install -U pytest
import pytest
from df_test import show_first_student_info_loc, show_first_student_info_iloc, show_all_student_names 
from df_test import merge_scores_to_info, set_missing_scores_to_N, show_male_students, show_male_students
from df_test import show_female_high_scores, sort_by_scores_desc, show_names_and_scores
from df_test import rename_score_to_exam1_score, add_pass_column

@pytest.fixture  
def data():
    data_info = {'student_id': ['1', '2', '3', '4', '5'],
             'name': ['Alice', 'Bob', 'Charlie', 'David', 'Eva'],
             'gender': ['F', 'M', 'M', 'M', 'F']}

    data_scores = {'student_id': [1, 2, 3, 4, 6],
                   'score': [85, 90, 78, 92, 88]}
    student_info = pd.DataFrame(data_info)
    student_scores = pd.DataFrame(data_scores)

    return student_info, student_scores

def test_show_first_student_info_loc(data):
    """
    顯示student_info第一個學生的基本資料
    請使用loc
    """
    student_info, student_scores = data
    actual = show_first_student_info_loc(student_info, student_scores)

    expected = student_info.loc[0]

    assert actual.equals(expected)



def test_show_first_student_info_iloc(data):
    """
    顯示student_info第一個學生的基本資料
    請使用iloc
    """
    student_info, student_scores = data
    actual = show_first_student_info_iloc(student_info, student_scores)

    expected = student_info.iloc[0]

    assert actual.equals(expected)

def test_show_all_student_names(data):
    """
    顯示所有學生的名字
    """
    student_info, student_scores = data
    actual = show_all_student_names(student_info, student_scores)

    expected = student_info[['name']]

    assert actual.equals(expected)

def test_merge_scores_to_info(data):
    """
    合併兩者每個學生都可以多一個欄顯示成績
    """
    student_info, student_scores = data
    actual = merge_scores_to_info(student_info, student_scores)

    student_info['student_id'] = student_info['student_id'].astype(int)
    expected = student_info.merge(student_scores, on='student_id', how='left')

    assert actual.equals(expected)

def test_set_missing_scores_to_N(data):
    """
    若沒有成績者df需顯示為"N"
    """
    student_info, student_scores = data
    actual = set_missing_scores_to_N(student_info, student_scores)

    student_info['student_id'] = student_info['student_id'].astype(int)
    expected = student_info.merge(student_scores, on='student_id', how='left')

    assert actual.equals(expected)

def test_show_male_students(data):
    """
    顯示合併後所有男學生的資料
    """
    student_info, student_scores = data
    actual = show_male_students(student_info, student_scores)

    student_info['student_id'] = student_info['student_id'].astype(int)
    merged_df = student_info.merge(student_scores, on='student_id', how='left')
    expected = merged_df[merged_df['gender'] == 'M']

    assert actual.equals(expected)

def test_show_female_high_scores(data):
    """
    顯示合併後所有女學生且分數高於等於90分的資料
    """
    student_info, student_scores = data
    actual = show_female_high_scores(student_info, student_scores)

    student_info['student_id'] = student_info['student_id'].astype(int)
    merged_df = student_info.merge(student_scores, on='student_id', how='left')
    expected = merged_df[(merged_df['gender'] == 'F') & (merged_df['score'] >= 90)]

    assert actual.equals(expected)

def test_sort_by_scores_desc(data):
    """
    將合併後的df依照分數高至低排序
    """
    student_info, student_scores = data
    actual = sort_by_scores_desc(student_info, student_scores)
    
    student_info['student_id'] = student_info['student_id'].astype(int)
    merged = student_info.merge(student_scores, on='student_id', how='left')
    expected = merged.sort_values(by='score', ascending=False)
    assert actual.equals(expected)

def test_names_and_scores(data):
    """
    只顯示學生姓名與成績
    """
    student_info, student_scores = data
    actual = show_names_and_scores(student_info, student_scores)

    student_info['student_id'] = student_info['student_id'].astype(int)
    merged_df = student_info.merge(student_scores, on='student_id', how='left')
    expected = merged_df[['name', 'score']]

    assert actual.equals(expected)

def test_rename_score_to_exam1_score(data):
    """
    顯示合併後的數列，但欄位score更改為 exam1_score
    """
    student_info, student_scores = data
    actual = rename_score_to_exam1_score(student_info, student_scores)
    student_info['student_id'] = student_info['student_id'].astype(int)
    merged_df = student_info.merge(student_scores, on='student_id', how='left')
    expected = merged_df.rename(columns={'score': 'exam1_score'})
    assert actual.equals(expected)

def test_add_pass_column(data):
    """
    新增一欄pass，若小於成績中位數顯示"fail",若大於成績中位數顯示“pass”
    """
    student_info, student_scores = data
    actual = add_pass_column(student_info, student_scores)

    student_info['student_id'] = student_info['student_id'].astype(int)
    merged_df = student_info.merge(student_scores, on='student_id', how='left')
    median_score = merged_df['score'].dropna().median()
    merged_df['pass'] = merged_df['score'].apply(lambda x: 'pass' if ((x >= median_score) and (pd.notnull(x))) else 'fail')
    expected = merged_df.copy()

    assert actual.equals(expected)

if __name__ == "__main__":
    pytest.main()