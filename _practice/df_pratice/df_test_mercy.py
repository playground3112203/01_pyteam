"""
題目：
有兩個 DataFrame，一個是存儲學生基本信息的student_info，另一個是存儲學生成績的 student_scores。
請在下列十個functions中回傳題目所要的值
有些題目不只一種做法
"""
import pandas as pd

data_info = {'student_id': ['1', '2', '3', '4', '5'],
             'name': ['Alice', 'Bob', 'Charlie', 'David', 'Eva'],
             'gender': ['F', 'M', 'M', 'M', 'F']}
student_info = pd.DataFrame(data_info)
data_scores = {'student_id': [1, 2, 3, 4, 6],
               'score': [85, 90, 78, 92, 88]}
student_scores = pd.DataFrame(data_scores)

import pandas as pd

def show_first_student_info_loc(student_info, student_scores):
    """
    1.顯示student_info第一個學生的基本資料
    請使用loc
    
    import pandas as pd
    
    data_info = {
        'student_id': ['1', '2', '3', '4', '5'],
        'name': ['Alice', 'Bob', 'Charlie', 'David', 'Eva'],
        'gender': ['F', 'M', 'M', 'M', 'F']}
    student_info = pd.DataFrame(data_info)
    data_scores = {
        'student_id': ['1', '2', '3', '4', '6'],
        'score': [85, 90, 78, 92, 88]}
    student_scores = pd.DataFrame(data_scores)
    
    a=pd.merge(student_info, student_scores, on="student_id")
    print(a.loc[ 0, 'student_id' : 'gender'])
    """
    ...
def show_first_student_info_iloc(student_info, student_scores):
    """
    2.顯示student_info第一個學生的基本資料
    請使用iloc
    
    import pandas as pd

    data_info = {
        'student_id': ['1', '2', '3', '4', '5'],
        'name': ['Alice', 'Bob', 'Charlie', 'David', 'Eva'],
        'gender': ['F', 'M', 'M', 'M', 'F']}
    student_info = pd.DataFrame(data_info)
    data_scores = {
        'student_id': ['1', '2', '3', '4', '6'],
        'score': [85, 90, 78, 92, 88]}
    student_scores = pd.DataFrame(data_scores)

    a=pd.merge(student_info, student_scores, on="student_id")
    print(a.iloc[ 0, 0:3 ])
    """
    ...

def show_all_student_names(student_info, student_scores):
    """
    3.顯示所有學生的名字
    
    import pandas as pd

    data_info = {
        'student_id': ['1', '2', '3', '4', '5'],
        'name': ['Alice', 'Bob', 'Charlie', 'David', 'Eva'],
        'gender': ['F', 'M', 'M', 'M', 'F']}
    student_info = pd.DataFrame(data_info)
    data_scores = {
        'student_id': ['1', '2', '3', '4', '5'],
        'score': [85, 90, 78, 92, 88]}
    student_scores = pd.DataFrame(data_scores)

    a=pd.merge(student_info, student_scores, on="student_id")
    print(a.loc[ 0:4,'name' ])
    """
    ...

def merge_scores_to_info(student_info, student_scores):
    """
    4.合併兩者每個學生都可以多一個欄顯示成績
    
    import pandas as pd

    data_info = {
        'student_id': ['1', '2', '3', '4', '5'],
        'name': ['Alice', 'Bob', 'Charlie', 'David', 'Eva'],
        'gender': ['F', 'M', 'M', 'M', 'F']}
    student_info = pd.DataFrame(data_info)
    data_scores = {
        'student_id': ['1', '2', '3', '4', '5'],
        'score': [85, 90, 78, 92, 88]}
    student_scores = pd.DataFrame(data_scores)

    print(pd.merge(student_info, student_scores, on="student_id"))
    """
    ...

def set_missing_scores_to_N(student_info, student_scores):
    """
    5.若沒有成績者df需顯示為"N"
    
    import pandas as pd

    data_info = {
        'student_id': ['1', '2', '3', '4', '5'],
        'name': ['Alice', 'Bob', 'Charlie', 'David', 'Eva'],
        'gender': ['F', 'M', 'M', 'M', 'F']}
    student_info = pd.DataFrame(data_info)

    data_scores = {
        'student_id': ['1', '2', '3', '4', '5'],
        'score': [85, 90, 0, 92, 88]}
    student_scores = pd.DataFrame(data_scores)

    student_scores['score'] = student_scores['score'].replace(0, 'N')

    result_df = pd.merge(student_info, student_scores, on="student_id")

    print(result_df)
    """
    ...

def show_male_students(student_info, student_scores):
    """
    6.顯示合併後所有男學生的資料
    
    import pandas as pd

    data_info = {
        'student_id': ['1', '2', '3', '4', '5'],
        'name': ['Alice', 'Bob', 'Charlie', 'David', 'Eva'],
        'gender': ['F', 'M', 'M', 'M', 'F']}
    student_info = pd.DataFrame(data_info)
    data_scores = {
        'student_id': ['1', '2', '3', '4', '5'],
        'score': [85, 90, 78, 92, 88]}
    student_scores = pd.DataFrame(data_scores)

    a=pd.merge(student_info, student_scores, on="student_id")

    print(a.loc[ 1:3, 'student_id' : 'score'])
    
    data_info = {
        'student_id': ['1', '2', '3', '4', '5'],
        'name': ['Alice', 'Bob', 'Charlie', 'David', 'Eva'],
        'gender': ['F', 'M', 'M', 'M', 'F']}
    student_info = pd.DataFrame(data_info)

    data_scores = {
        'student_id': ['1', '2', '3', '4', '5'],
        'score': [85, 90, 78, 92, 88]}
    student_scores = pd.DataFrame(data_scores)

    merged_df = pd.merge(student_info, student_scores, on="student_id")
    male_data = merged_df[merged_df['gender'] == 'M']

    print(male_data)
    """
    ...

def show_female_high_scores(student_info, student_scores):
    """
    7.顯示合併後所有女學生且分數高於等於90分的資料
    
    import pandas as pd

    data_info = {
        'student_id': ['1', '2', '3', '4', '5'],
        'name': ['Alice', 'Bob', 'Charlie', 'David', 'Eva'],
        'gender': ['F', 'M', 'M', 'M', 'F']}
    student_info = pd.DataFrame(data_info)

    data_scores = {
        'student_id': ['1', '2', '3', '4', '5'],
        'score': [85, 90, 78, 92, 88]}
    student_scores = pd.DataFrame(data_scores)

    merged_df = pd.merge(student_info, student_scores, on="student_id")

    female_data_above_90 = merged_df[(merged_df['gender'] == 'F') & (merged_df['score'] > 90)]

    if female_data_above_90.empty:
        print('F')
    else:
        print(female_data_above_90)
    """
    ...

def sort_by_scores_desc(student_info, student_scores):
    """
    8.將合併後的df依照分數高至低排序
    
    import pandas as pd

    data_info = {
        'student_id': ['1', '2', '3', '4', '5'],
        'name': ['Alice', 'Bob', 'Charlie', 'David', 'Eva'],
        'gender': ['F', 'M', 'M', 'M', 'F']}
    student_info = pd.DataFrame(data_info)

    data_scores = {
        'student_id': ['1', '2', '3', '4', '5'],
        'score': [85, 90, 78, 92, 88]}
    student_scores = pd.DataFrame(data_scores)

    merged_df = pd.merge(student_info, student_scores, on="student_id")
    sorted_df = merged_df.sort_values(by='score', ascending=False)

    print(sorted_df)
    """
    ...

def show_names_and_scores(student_info, student_scores):
    """
    9.只顯示學生姓名與成績
    
    import pandas as pd

    data_info = {
        'student_id': ['1', '2', '3', '4', '5'],
        'name': ['Alice', 'Bob', 'Charlie', 'David', 'Eva'],
        'gender': ['F', 'M', 'M', 'M', 'F']}
    student_info = pd.DataFrame(data_info)
    data_scores = {
        'student_id': ['1', '2', '3', '4', '5'],
        'score': [85, 90, 78, 92, 88]}
    student_scores = pd.DataFrame(data_scores)

    a=pd.merge(student_info, student_scores, on="student_id")

    print(a.loc[ 0:4, ['name','score']])
    """
    ...

def rename_score_to_exam1_score(student_info, student_scores):
    """
    10.顯示合併後的數列，但欄位score更改為 exam1_score
    
    import pandas as pd

    data_info = {
        'student_id': ['1', '2', '3', '4', '5'],
        'name': ['Alice', 'Bob', 'Charlie', 'David', 'Eva'],
        'gender': ['F', 'M', 'M', 'M', 'F']}
    student_info = pd.DataFrame(data_info)

    data_scores = {
        'student_id': ['1', '2', '3', '4', '5'],
        'score': [85, 90, 78, 92, 88]}
    student_scores = pd.DataFrame(data_scores)

    merged_df = pd.merge(student_info, student_scores, on="student_id")

    merged_df = merged_df.rename(columns={'score': 'exam1_score'})

    print(merged_df)
    """
    ...

def add_pass_column(student_info, student_scores):
    """
    11.新增一欄pass，若小於成績中位數顯示"fail",若大於成績中位數顯示“pass”
    另外最後一題，我發現我出的方式有一點問題
    可以把沒有分數的當成0計算中位數或不把沒有分數的當成中位數，我補充一下題目：
    中位數不計入沒有分數的值，若沒有分數直接當"fail"
    
    import pandas as pd

    data_info = {
        'student_id': ['1', '2', '3', '4', '5'],
        'name': ['Alice', 'Bob', 'Charlie', 'David', 'Eva'],
        'gender': ['F', 'M', 'M', 'M', 'F']}
    student_info = pd.DataFrame(data_info)

    data_scores = {
        'student_id': ['1', '2', '3', '4', '5'],
        'score': [85, 90, 78, 92, 88]}
    student_scores = pd.DataFrame(data_scores)

    merged_df = pd.merge(student_info, student_scores, on="student_id")

    median_score = merged_df['score'].median()

    merged_df['pass'] = 'fail'
    merged_df.loc[merged_df['score'] > median_score, 'pass'] = 'pass'

    print(merged_df)
    """
    ...
