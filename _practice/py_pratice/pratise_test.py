# pip install -U pytest
import pytest
#%%
from python_simple_test import print_christmas_tree

def test_christmas_tree_height_3(capsys):
    print_christmas_tree(3)
    captured = capsys.readouterr()
    assert captured.out == "  *\n ***\n*****\n  *\n"

def test_christmas_tree_height_5(capsys):
    print_christmas_tree(5)
    captured = capsys.readouterr()
    assert captured.out == "    *\n   ***\n  *****\n *******\n*********\n    *\n"

def test_christmas_tree_height_1(capsys):
    print_christmas_tree(1)
    captured = capsys.readouterr()
    assert captured.out == "*\n"

def test_christmas_tree_height_0(capsys):
    print_christmas_tree(0)
    captured = capsys.readouterr()
    assert captured.out == ""

#%%
from python_simple_test import is_leap_year
# 測試閏年
def test_leap_years():
    assert is_leap_year(2000) == True
    assert is_leap_year(2020) == True
    assert is_leap_year(2400) == True
    assert is_leap_year(1600) == True

# 測試平年
def test_non_leap_years():
    assert is_leap_year(1900) == False
    assert is_leap_year(2100) == False
    assert is_leap_year(2200) == False
    assert is_leap_year(2300) == False

# 測試一般年份
def test_regular_years():
    assert is_leap_year(2022) == False
    assert is_leap_year(2023) == False
    assert is_leap_year(2024) == True
    assert is_leap_year(2025) == False

# 測試負數年份
def test_negative_years():
    with pytest.raises(ValueError):
        is_leap_year(-2000)

# 測試非整數年份
def test_non_integer_years():
    with pytest.raises(ValueError):
        is_leap_year(2020.5)

#%%
from python_simple_test import collatz_sequence

def test_collatz_sequence_starting_with_6():
    assert collatz_sequence(6) == [6, 3, 10, 5, 16, 8, 4, 2, 1]

def test_collatz_sequence_starting_with_13():
    assert collatz_sequence(13) == [13, 40, 20, 10, 5, 16, 8, 4, 2, 1]

def test_collatz_sequence_starting_with_1():
    assert collatz_sequence(1) == [1]

def test_collatz_sequence_starting_with_22():
    assert collatz_sequence(22) == [22, 11, 34, 17, 52, 26, 13, 40, 20, 10, 5, 16, 8, 4, 2, 1]

def test_collatz_sequence_starting_with_27():
    assert collatz_sequence(27) == [27, 82, 41, 124, 62, 31, 94, 47, 142, 71, 214, 107, 322, 161, 484, 242, 121, 364, 182, 91, 274, 137, 412, 206, 103, 310, 155, 466, 233, 700, 350, 175, 526, 263, 790, 395, 1186, 593, 1780, 890, 445, 1336, 668, 334, 167, 502, 251, 754, 377, 1132, 566, 283, 850, 425, 1276, 638, 319, 958, 479, 1438, 719, 2158, 1079, 3238, 1619, 4858, 2429, 7288, 3644, 1822, 911, 2734, 1367, 4102, 2051, 6154, 3077, 9232, 4616, 2308, 1154, 577, 1732, 866, 433, 1300, 650, 325, 976, 488, 244, 122, 61, 184, 92, 46, 23, 70, 35, 106, 53, 160, 80, 40, 20, 10, 5, 16, 8, 4, 2, 1]

#%%
from python_simple_test import factorize

def test_factorize_60():
    assert factorize(60) == {2: 2, 3: 1, 5: 1}

def test_factorize_72():
    assert factorize(72) == {2: 3, 3: 2}

def test_factorize_90():
    assert factorize(90) == {2: 1, 3: 2, 5: 1}

def test_factorize_120():
    assert factorize(120) == {2: 3, 3: 1, 5: 1}

def test_factorize_17():
    assert factorize(17) == {17: 1}

#%%
from python_simple_test import is_palindrome

def test_is_palindrome_level():
    assert is_palindrome("level") == True

def test_is_palindrome_python():
    assert is_palindrome("python") == False

def test_is_palindrome_racecar():
    assert is_palindrome("racecar") == True

def test_is_palindrome_madam():
    assert is_palindrome("madam") == True

def test_is_palindrome_hello():
    assert is_palindrome("hello") == False

#%%
from python_simple_test import list_files

def test_list_files_in_directory(tmpdir):
    # 在臨時目錄中創建一些檔案和子目錄
    tmpdir.mkdir("subdir")
    tmpdir.join("file1.txt").write("")
    tmpdir.join("file2.jpg").write("")
    tmpdir.join("subdir/file3.txt").write("")

    # 測試列出檔案
    result = list_files(str(tmpdir))
    
    assert "file1.txt" in result
    assert "file2.jpg" in result
    assert "subdir" in result
    assert "subdir/file3.txt" not in result

#%%
from python_simple_test import add_days_to_date

def test_add_days_to_date():
    # 一般
    result = add_days_to_date('2023-05-25', 7)
    assert result == '2023-06-01'

    # 負天數
    result = add_days_to_date('2023-05-25', -3)
    assert result == '2023-05-22'

    # 0天數
    result = add_days_to_date('2023-05-25', 0)
    assert result == '2023-05-25'

    # 跨月份
    result = add_days_to_date('2023-05-25', 10)
    assert result == '2023-06-04'

#%%
from python_simple_test import process_numbers

def test_process_numbers():
    """
    max, min, length, sorted_list, total, max_abs, sum_of_squares
    """
    numbers = [3, -7, 2, 8, -4]

    result = process_numbers(numbers)
    assert result == [8, -7, 5, [-7, -4, 2, 3, 8], 2, -8, 118]

#%%
from python_simple_test import with_if_function, with_if_statement

def test_with_if_statement():
    result = with_if_statement()
    assert result == 47

def test_with_if_function():
    result = with_if_function()
    assert result == [42, 47]
