"""
python 10題練習
"""
def sum_of_odd_numbers(n:int)->int:
    """
    if 練習
    接受一個正整數 n，然後計算並返回所有小於等於 n 的奇數的和

    >>> sum_of_odd_numbers(10) #  1+3+5+7+9
    25
    >>> sum_of_odd_numbers(15) # 1+3+5+7+9+11+13+15
    64
    >>> sum_of_odd_numbers(5) # 1+3+5
    9
    """
    pass

#%%
def print_christmas_tree(height:int)->str:
    """
    for while string 練習
    return一顆聖誕樹。
    參數：
    - height: 樹的高度，整數型別。
    輸出：
    - 聖誕樹的形狀，使用 '*' 字符。

    例子：
    >>> print_christmas_tree(3)
      *
     ***
    *****
      *

    >>> print_christmas_tree(5)
        *
       ***
      *****
     *******
    *********
        *
    """
    pass

#%%
def is_leap_year(year: int) -> bool:
    """
    for if 練習
    格瑞哥里的煩惱
    現行的曆法是從羅馬人的曆法演變而來的。凱撒大帝編纂了一套曆法，後人稱之為儒略曆 (Julian calendar)。
    在這曆法中，除了四、六、九、及十一月有30天，二月在平年有28天，在閏年有29天以外，其他的月份都是31天。
    再者，在這曆法中，每四年有一個閏年。這導因於古代羅馬的星象學家算出一年有365.25天，因此每隔四年就要加一天以保持曆法和季節的一致。
    於是，他們就在四的倍數的年份多加了一天 (二月29日)。
    儒略法：
    四的倍數的年份均為閏年，這年會多一天 (二月29日)。
    在1582年，教宗格瑞哥里 (Gregory) 的星象學家發現一年並不是365.25天，而是比較接近365.2425天。因此，閏年的規則便修正如下：
    格瑞哥里法：
    除了不是400的倍數的100的倍數以外，四的倍數的年份均為閏年。
    為了要彌補截至當時季節和日曆已產生的誤差，當時的日曆便往前挪移了10天：1582年10月4日的第二天為10月15日。
    英格蘭和它的帝國 (包括美國) 一直到1752年才改用格瑞哥里曆，當年的9月2日的第二天為9月14日。(未同步採用新曆乃肇因於亨利八世和教宗的惡劣關係。)
    請依現行的曆法判斷所給的西元年份是平年還是閏年。

    >>> is_leap_year(4)
    True
    >>> is_leap_year(1992)
    True
    >>> is_leap_year(1993)
    False
    >>> is_leap_year(1900)
    False
    >>> is_leap_year(2000)
    True
    """
    pass


#%%
def collatz_sequence(n:int)->list:
    """
    for if list 練習
    3n + 1 猜想為
    1. 輸入 n
    2. 印出 n
    3. 如果 n = 1 結束
    4. 如果 n 是奇數 那麼 n=3*n+1
    5. 否則 n=n/2
    6. GOTO 2
    例如輸入 22, 得到的數列： 22 11 34 17 52 26 13 40 20 10 5 16 8 4 2 1 
    參數：
    - n: 起始數字，正整數。
    返回：
    - 一個列表，包含 Collatz 序列的所有數字，包括起始數字和終止於 1。

    例子：
    >>> collatz_sequence(6)
    [6, 3, 10, 5, 16, 8, 4, 2, 1]
    >>> collatz_sequence(13)
    [13, 40, 20, 10, 5, 16, 8, 4, 2, 1]
    """
    pass


#%%
def factorize(n:int)->dict:
    """
    string dict 練習
    將給定的正整數 n 進行因數分解。

    參數：
    - n: 正整數。

    返回：
    - 一個字典，包含 n 的因數分解結果。字典的鍵是質因數，值是該質因數的次數。

    例子：
    >>> factorize(60)
    {2: 2, 3: 1, 5: 1}
    # 60 = 2^2 * 3 * 5
    """
    pass

#%%
def is_palindrome(s:str)->bool:
    """
    string 練習
    判斷給定的字符串是否為回文。
    參數：
    - s: 字符串。
    返回：
    - 如果 s 是回文，返回 True；否則返回 False。

    例子：
    >>> is_palindrome("level")
    True
    >>> is_palindrome("python")
    False
    """
    pass

#%%
import os
def list_files(directory:str)->list:
    """
    內建函數os for list練習
    列出指定目錄下的所有檔案。

    參數：
    - directory: 要列出檔案的目錄。

    返回：
    - 一個包含目錄中所有檔案名的列表。

    例子：
    >>> list_files("/path/to/directory")
    ['file1.txt', 'file2.jpg', 'subdirectory', ...]
    """
    pass

#%%
import datetime
def add_days_to_date(date_str:str, days:int)->int:
    """
    內建函數datetime練習
    接受一個日期字符串和一個整數（代表天數），返回指定天數後的日期。

    參數：
    - date_str: 日期字符串，格式為 'YYYY-MM-DD'。
    - days: 要添加的天數。

    返回：
    - 一個字符串，表示計算後的日期。

    例子：
    >>> add_days_to_date('2023-05-25', 7)
    '2023-06-01'
    """
    pass

#%%
def process_numbers(numbers:list)->list:
    """
    python內建函數
    接受一個整數列表 numbers，並執行以下操作：
    1. 返回列表中的最大數。
    2. 返回列表中的最小數。
    3. 返回列表的長度。
    4. 返回排序後的列表。
    5. 返回列表中所有數字的總和。
    6. 返回列表中絕對值最大的數字。
    7. 返回列表中所有數字的平方和。

    參數：
    - numbers: 一個包含整數的列表。

    返回：
    一個包含以上計算結果的列表，順序為 max, min, length, sorted_list, total, max_abs, sum_of_squares。
    """
    pass


#%% callback練習
# 以下為一個function 題組練習
def if_function(condition, true_result, false_result):
    """如果條件為真，返回 true_result，否則返回 false_result。
    >>> if_function(True, 2, 3)
    2
    >>> if_function(False, 2, 3)
    3
    >>> if_function(3==2, 3+2, 3-2)
    1
    >>> if_function(3>2, 3+2, 3-2)
    5
    """
    if condition:
        return true_result
    else:
        return false_result

"""
儘管有上述文檔測試，但此函數實際上並非在所有情況下都與 if 語句相同。
為了證明這一點，請編寫函數 cond、true_func 和 false_func，使 with_if_statement 只打print數字 47，而 with_if_function print 42 和 47 兩個數字。
"""
def with_if_statement():
    """
    >>> result = with_if_statement()
    47
    >>> print(result)
    None
    """
    if cond():
        return true_func()
    else:
        return false_func()

def with_if_function():
    """
    >>> result = with_if_function()
    42
    47
    >>> print(result)
    None
    """
    return if_function(cond(), true_func(), false_func())

def cond():
    "*** 請在此處編寫您的代碼 ***"

def true_func():
    "*** 請在此處編寫您的代碼 ***"

def false_func():
    "*** 請在此處編寫您的代碼 ***"
