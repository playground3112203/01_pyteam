# -*- coding: utf-8 -*-
import pandas as pd
import yaml #pip install pyyaml

with open('../config.yml', 'r', encoding='utf-8') as stream:
    myconfig = yaml.load(stream, Loader=yaml.CLoader)

##---- log參數設定
import logging
FORMAT = '%(asctime)s [%(levelname)s]: %(message)s'
logging.basicConfig(level=logging.INFO, format=FORMAT)

##---- loading data
df_prd = pd.read_csv(myconfig['shell_vars']['file_path_df_prd'])
df_prd['prd_name_seg'] = df_prd['prd_name_seg'].apply(lambda x: x.replace("['","").replace("']","").split("', '") )#還原

''' 下面這邊是df_prd誕生的過程，但因為牽涉 DB Query，所以就不讓你執行了。僅供參考~
df_prd的欄位：['pid', 'prd_name', 'kws_ori_output', 'catg_name', 'brand',
       'prd_name_seg']

pid: 商品序號
prd_name：商品名稱
kws_ori_output：CKIP斷詞後的關鍵字
catg_name：商品底層目錄(分類)
brand：品牌
prd_name_seg：整理後的資料 = kws_ori_output + catg_name + brand
'''

sent = [row for row in df_prd['prd_name_seg']]

##---- word2vec
##---- Training
import multiprocessing
from gensim.models import FastText
# pip install gensim
# from gensim.models.phrases import Phrases, Phraser
from time import time 

cores = multiprocessing.cpu_count() # Count the number of cores in a computer

t = time()
ft_model = FastText(sent, vector_size=200, window=5, min_count=2, sg=1)
logging.info('Time to train the model_Build_Vocab : {} mins'.format(round((time() - t) / 60, 2)))

len_str = str(len(ft_model.wv.index_to_key))
logging.info('Original FastText word index numbers: '+ len_str ) 

len_str = str(len(ft_model.wv.index_to_key))
logging.info('After updateding, FastText word index numbers: '+ len_str ) 

##--- Use
# ## save
# ft_model.save("ft_model.model")
# logging.info('Job finished.')

# ## load
# ft_model = FastText.load("ft_model.model")

# ## get model info for elasticsearch
# #  實際使用上，會只把文字的向量資訊抽出來。所以如果要換model，也要可以抽出文字的向量資料~
# vocab = ft_model.wv.index_to_key
# word_vectors = {word: ft_model.wv[str(word)].tolist() for word in vocab} # 這是最後被我存起來用的結構

ft_model.wv.most_similar(['掛繩']) #效果還不錯
ft_model.wv.most_similar(['荔枝']) #效果挺糟糕 (理想情況下，至少要有出現：玉荷包)
ft_model.wv.most_similar(['防水披肩']) #效果挺糟糕
